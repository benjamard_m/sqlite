import UIKit
import SQLite

class ViewController: UIViewController {

  @IBOutlet weak var listUserLabel: UILabel!
  var database: Connection!
  
  
  let usersTable = Table("users")
  let id = Expression<Int>("id")
  let name = Expression<String>("name")
  let email = Expression<String>("email")
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    do {
      let documentDirectory = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
      let fileUrl = documentDirectory.appendingPathComponent("users").appendingPathExtension("sqlite3")
      let database = try Connection(fileUrl.path)
      self.database = database
    } catch {
      print(error)
    }
  }

  @IBAction func onClickCreateTable(_ sender: Any) {
    print("CREATE TAPPED: Create users table with three column")
    let createTable = self.usersTable.create { (table) in
      table.column(self.id, primaryKey: true)
      table.column(self.name)
      table.column(self.email, unique: true)
    }
    do {
      try self.database.run(createTable)
      print("Created Table")
      self.alertSuccess(message: "Success created!")
    } catch {
      print(error)
    }
  }
  
  @IBAction func onClickCreateUser(_ sender: Any) {
    print("INSERT TAPPED")
    let alert = UIAlertController(title: "Insert User", message: nil, preferredStyle: .alert)
    alert.addTextField { (tf) in tf.placeholder = "Name" }
    alert.addTextField { (tf) in tf.placeholder = "Email" }
    let action = UIAlertAction(title: "Submit", style: .default) { (_) in
      guard let name = alert.textFields?.first?.text,
        let email = alert.textFields?.last?.text
        else { return }
      let insertUser = self.usersTable.insert(self.name <- name, self.email <- email)
      
      do {
        try self.database.run(insertUser)
        print("INSERTED USER: name: \(name) \n email: \(email)")
        self.alertSuccess(message: "Success inserted! name: \(name) \n email: \(email)")
      } catch {
        print(error)
      }
      
    }
    alert.addAction(action)
    present(alert, animated: true, completion: nil)
  }
  @IBAction func onClickUpdateUser(_ sender: Any) {
    print("UPDATE TAPPED")
    let alert = UIAlertController(title: "Update User", message: nil, preferredStyle: .alert)
    alert.addTextField { (tf) in tf.placeholder = "User ID" }
    alert.addTextField { (tf) in tf.placeholder = "Email" }
    let action = UIAlertAction(title: "Submit", style: .default) { (_) in
      guard let userIdString = alert.textFields?.first?.text,
        let userId = Int(userIdString),
        let email = alert.textFields?.last?.text
        else { return }
      let user = self.usersTable.filter(self.id == userId)
      let updateUser = user.update(self.email <- email)
      do {
        try self.database.run(updateUser)
        self.alertSuccess(message: "Success updated! userId: \(userId) \n email: \(email)")
      } catch {
        print(error)
      }
    }
    alert.addAction(action)
    present(alert, animated: true, completion: nil)
  }
  @IBAction func onClickDeleteUser(_ sender: Any) {
    print("DELETE TAPPED")
    let alert = UIAlertController(title: "Update User", message: nil, preferredStyle: .alert)
    alert.addTextField { (tf) in tf.placeholder = "User ID" }
    let action = UIAlertAction(title: "Submit", style: .default) { (_) in
      guard let userIdString = alert.textFields?.first?.text,
        let userId = Int(userIdString)
        else { return }
      print(userIdString)
      
      let user = self.usersTable.filter(self.id == userId)
      let deleteUser = user.delete()
      do {
        try self.database.run(deleteUser)
        self.alertSuccess(message: "Success deleted! userId: \(userIdString)")
      } catch {
        print(error)
      }
    }
    alert.addAction(action)
    present(alert, animated: true, completion: nil)
  }
  @IBAction func onClickListUser(_ sender: Any) {
    do {
      let users = try self.database.prepare(self.usersTable)
      for user in users {
        print("userId: \(user[self.id]), name: \(user[self.name]), email: \(user[self.email])")
        listUserLabel.text = "userId: \(user[self.id]), name: \(user[self.name]), email: \(user[self.email])"
      }
    } catch {
      print(error)
    }
  }
  
  
  func alertSuccess(message: String) {
    let alertVC = UIAlertController(title: "Success!", message: message, preferredStyle: .actionSheet)
    let button = UIAlertAction(title: "OK", style: .default, handler: nil)
    alertVC.addAction(button)
    self.present(alertVC, animated: true, completion: nil)
  }
}

